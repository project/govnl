# GovNL Events
Deze module installeert het Event (govnl_event) content type en de bijbehorende configuratie.
Onder deze configuratie vallen de standaard configuratie voor het content type.
Ook wordt er een View Block aangemaakt die een lijst met aankomende events toont.

## Dependencies
Om deze module te kunnen gebruiken dienen een aantal dependencies worden geinstalleerd:
- menu_ui
- node
- text
- view
- path
- datetime
